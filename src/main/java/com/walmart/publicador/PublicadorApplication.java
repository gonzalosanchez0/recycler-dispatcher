package com.walmart.publicador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.walmart.publicador.controller.GreetServer;

@SpringBootApplication
public class PublicadorApplication implements CommandLineRunner {
	
	@Autowired
	GreetServer greet;
	
	private final int port = 708;

	public static void main(String[] args) {
		SpringApplication.run(PublicadorApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		greet.start(port);
	}

}
