package com.walmart.publicador.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.walmart.publicador.model.RemoteMessage;
import com.walmart.publicador.service.KafkaProducerMessage;

@Component
public class GreetServer {

	Logger logger = LoggerFactory.getLogger(GreetServer.class);

	private ServerSocket serverSocket;
	private Socket clientSocket;
	private BufferedReader in;
	private PrintWriter out;
	private String xml = null;
	private final String xmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

	@Autowired
	private KafkaProducerMessage sender;

	public void start(int port) throws IOException {
		String inputLine = null;
		serverSocket = new ServerSocket(port);
		logger.info("El proceso se encuentra a la escucha...");
		do {
			clientSocket = serverSocket.accept();
			in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			while ((inputLine = in.readLine()) != null && !".".equals(inputLine)) {
				sendToKafka(inputLine);
			}
		} while (!".".equals(inputLine));
		logger.info("El proceso ha sido finalizado por el usuario.");
	}

	private void sendToKafka(String message) {
		if (xmlHeader.equals(message)) {
			xml = message;
		} else {
			xml = xml + message;
			try {
				if (sender.sendMessage(xml)) {
					out = new PrintWriter(clientSocket.getOutputStream(), true);
					out.println(xmlHeader + "<RemoteMessageAck ID=\"" + xmlStringToRemoteMessage(xml).getNop() + "\"></RemoteMessageAck>");
				}
			} catch (Exception e) {
				logger.error("Error: " + e.getMessage());
			}
		}
	}

	private RemoteMessage xmlStringToRemoteMessage(String xml) throws IOException {
		return new XmlMapper().readValue(xml, RemoteMessage.class);
	}

}