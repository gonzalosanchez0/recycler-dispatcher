package com.walmart.publicador.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Component
public class KafkaProducerMessage {

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	Logger logger = LoggerFactory.getLogger(KafkaProducerMessage.class);

	private boolean senderResult = false;
	private final String topic = "_storesystems.recycler.log";

	public boolean sendMessage(String message) {
		ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(topic, message);
		future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
			@Override
			public void onSuccess(SendResult<String, String> result) {
				logger.info("Sent message=[" + message + "] with offset=[" + result.getRecordMetadata().offset() + "]");
				senderResult = true;
			}

			@Override
			public void onFailure(Throwable ex) {
				logger.error("Unable to send message=[" + message + "] due to : " + ex.getMessage());
				senderResult = false;
			}
		});
		return senderResult;
	}

}
