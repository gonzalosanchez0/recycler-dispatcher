package com.walmart.publicador.model;

import java.util.Arrays;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "RemoteMessage")
public class RemoteMessage {
	@JacksonXmlProperty(localName = "operation", isAttribute = true)
	private String operation;
	@JacksonXmlProperty(localName = "DeviceID", isAttribute = true)
	private String deviceId;
	@JacksonXmlProperty(localName = "CustomerCode", isAttribute = true)
	private String customerCode;
	@JacksonXmlProperty(localName = "NOP", isAttribute = true)
	private String nop;
	@JacksonXmlProperty(localName = "Date", isAttribute = true)
	private String date;
	@JacksonXmlProperty(localName = "Time", isAttribute = true)
	private String time;
	@JacksonXmlProperty(localName = "User")
	private String user;
	@JacksonXmlProperty(localName = "UserName")
	private String userName;
	@JacksonXmlProperty(localName = "ExecutedBy")
	private String executedBy;
	@JacksonXmlProperty(localName = "UserAlternateID")
	private String userAlternateId;
	@JacksonXmlProperty(localName = "UserOrganization")
	private String userOrganization;
	@JacksonXmlProperty(localName = "TransactionID")
	private String transactionId;
	@JacksonXmlProperty(localName = "UserLevel")
	private String userLevel;
	@JacksonXmlProperty(localName = "ChannelID")
	private String channelId;
	@JacksonXmlProperty(localName = "ChannelName")
	private String channelName;
	@JacksonXmlProperty(localName = "shiftNumber")
	private String shiftNumber;
	@JacksonXmlProperty(localName = "DepositDuration")
	private String depositDuration;
	@JacksonXmlProperty(localName = "shiftEnd")
	private String shiftEnd;
	@JacksonXmlProperty(localName = "envelopeID")
	private String envelopeID;
	@JacksonXmlProperty(localName = "bagID")
	private String bagId;
	@JacksonXmlProperty(localName = "Conciliation")
	private String conciliation;
	@JacksonXmlProperty(localName = "customTrCode")
	private String customTrCode;
	@JacksonXmlProperty(localName = "isKit")
	private String isKit;
	@JacksonXmlProperty(localName = "Details")
	Details details;
	@JacksonXmlProperty(localName = "content")
	Content content;
	@JacksonXmlProperty(localName = "devStatus")
	DevStatus devStatus;
	@JacksonXmlProperty(localName = "software")
	Software software;
	@JacksonXmlProperty(localName = "BDM")
	Bdm bdm;
	@JacksonXmlProperty(localName = "CDM")
	Cdm cdm;

	@Override
	public String toString() {
		return "RemoteMessage [operation=" + operation + ", deviceId=" + deviceId + ", customerCode=" + customerCode
				+ ", nop=" + nop + ", date=" + date + ", time=" + time + ", user=" + user + ", userName=" + userName
				+ ", executedBy=" + executedBy + ", userAlternateId=" + userAlternateId + ", userOrganization="
				+ userOrganization + ", transactionId=" + transactionId + ", userLevel=" + userLevel + ", channelId="
				+ channelId + ", channelName=" + channelName + ", shiftNumber=" + shiftNumber + ", depositDuration="
				+ depositDuration + ", shiftEnd=" + shiftEnd + ", envelopeID=" + envelopeID + ", bagId=" + bagId
				+ ", conciliation=" + conciliation + ", customTrCode=" + customTrCode + ", isKit=" + isKit
				+ ", details=" + details + ", content=" + content + ", devStatus=" + devStatus + ", software="
				+ software + ", bdm=" + bdm + ", cdm=" + cdm + "]";
	}

	public String getNop() {
		return nop;
	}

}

class Details {
	@JacksonXmlProperty(localName = "Currency", isAttribute = true)
	private String currency;
	@JacksonXmlProperty(localName = "countings")
	Countings countings;
	@JacksonXmlProperty(localName = "Account")
	private String account;
	@JacksonXmlProperty(localName = "total")
	private String total;

	@Override
	public String toString() {
		return "Details [countings=" + countings + ", account=" + account + ", total=" + total + ", currency="
				+ currency + "]";
	}

}

class Countings {
	@JacksonXmlProperty(localName = "valid", isAttribute = true)
	private String valid;
	@JacksonXmlProperty(localName = "counted")
	@JacksonXmlElementWrapper(useWrapping = false)
	Counted[] counted;

	@Override
	public String toString() {
		return "Countings [counted=" + Arrays.toString(counted) + ", valid=" + valid + "]";
	}

}

class Counted {
	@JacksonXmlProperty(localName = "denom", isAttribute = true)
	private String denom;
	@JacksonXmlProperty(localName = "quantity", isAttribute = true)
	private String quantity;
	@JacksonXmlProperty(localName = "type", isAttribute = true)
	private String type;

	@Override
	public String toString() {
		return "Counted [denom=" + denom + ", quantity=" + quantity + ", type=" + type + "]";
	}

}

class Content {
	@JacksonXmlProperty(localName = "count")
	@JacksonXmlElementWrapper(useWrapping = false)
	Count[] count;

	@Override
	public String toString() {
		return "Content [count=" + Arrays.toString(count) + "]";
	}

}

class Count {
	@JacksonXmlProperty(localName = "den", isAttribute = true)
	private String den;
	@JacksonXmlProperty(localName = "curr", isAttribute = true)
	private String curr;
	@JacksonXmlProperty(localName = "qty", isAttribute = true)
	private String qty;
	@JacksonXmlProperty(localName = "type", isAttribute = true)
	private String type;
	@JacksonXmlProperty(localName = "N", isAttribute = true)
	private String n;
	@JacksonXmlProperty(localName = "sType", isAttribute = true)
	private String sType;

	@Override
	public String toString() {
		return "Count [den=" + den + ", curr=" + curr + ", qty=" + qty + ", type=" + type + ", n=" + n + ", sType="
				+ sType + "]";
	}

}

class DevStatus {
	@JacksonXmlProperty(localName = "dev")
	@JacksonXmlElementWrapper(useWrapping = false)
	Dev[] dev;

	@Override
	public String toString() {
		return "DevStatus [dev=" + Arrays.toString(dev) + "]";
	}

}

class Dev {
	@JacksonXmlProperty(localName = "name", isAttribute = true)
	private String name;
	@JacksonXmlProperty(localName = "mod", isAttribute = true)
	private String mod;
	@JacksonXmlProperty(localName = "err", isAttribute = true)
	private String err;
	@JacksonXmlProperty(localName = "clean", isAttribute = true)
	private String clean;
	@JacksonXmlProperty(localName = "door", isAttribute = true)
	private String door;
	@JacksonXmlProperty(localName = "devS", isAttribute = true)
	private String devS;
	@JacksonXmlProperty(localName = "bag", isAttribute = true)
	private String bag;
	@JacksonXmlProperty(localName = "cov", isAttribute = true)
	private String cov;
	@JacksonXmlProperty(localName = "blk", isAttribute = true)
	private String blk;
	@JacksonXmlProperty(localName = "bund", isAttribute = true)
	private String bund;
	@JacksonXmlProperty(localName = "ext", isAttribute = true)
	private String ext;

	@Override
	public String toString() {
		return "Dev [name=" + name + ", mod=" + mod + ", err=" + err + ", clean=" + clean + ", door=" + door + ", devS="
				+ devS + ", bag=" + bag + ", cov=" + cov + ", blk=" + blk + ", bund=" + bund + ", ext=" + ext + "]";
	}

}

class Software {
	@JacksonXmlProperty(localName = "sw")
	@JacksonXmlElementWrapper(useWrapping = false)
	Sw[] sw;

	@Override
	public String toString() {
		return "Software [sw=" + Arrays.toString(sw) + "]";
	}

}

class Sw {
	@JacksonXmlProperty(localName = "name", isAttribute = true)
	private String name;
	@JacksonXmlProperty(localName = "version", isAttribute = true)
	private String version;

	@Override
	public String toString() {
		return "Sw [name=" + name + ", version=" + version + "]";
	}

}

class Bdm {
	@JacksonXmlProperty(localName = "fw")
	Fw fw;
	@JacksonXmlProperty(localName = "RecFw")
	RecFw recFw;
	@JacksonXmlProperty(localName = "model")
	Model model;
	@JacksonXmlProperty(localName = "stocksConfig")
	StocksConfig stocksConfig;

	@Override
	public String toString() {
		return "Bdm [fw=" + fw + ", recFw=" + recFw + ", model=" + model + ", stocksConfig=" + stocksConfig + "]";
	}

}

class Cdm {
	@JacksonXmlProperty(localName = "fw")
	Fw fw;
	@JacksonXmlProperty(localName = "RecFw")
	RecFw recFw;
	@JacksonXmlProperty(localName = "model")
	Model model;
	@JacksonXmlProperty(localName = "stocksConfig")
	StocksConfig stocksConfig;

	@Override
	public String toString() {
		return "Cdm [fw=" + fw + ", recFw=" + recFw + ", model=" + model + ", stocksConfig=" + stocksConfig + "]";
	}

}

class Fw {
	@JacksonXmlProperty(localName = "version", isAttribute = true)
	private String version;

	@Override
	public String toString() {
		return "Fw [version=" + version + "]";
	}

}

class RecFw {
	@JacksonXmlProperty(localName = "version", isAttribute = true)
	private String version;

	@Override
	public String toString() {
		return "RecFw [version=" + version + "]";
	}

}

class Model {
	@JacksonXmlProperty(localName = "name", isAttribute = true)
	private String name;

	@Override
	public String toString() {
		return "Model [name=" + name + "]";
	}

}

class StocksConfig {
	@JacksonXmlElementWrapper(useWrapping = false)
	@JacksonXmlProperty(localName = "stock")
	Stock[] stock;

	@Override
	public String toString() {
		return "StockConfig [stock=" + Arrays.toString(stock) + "]";
	}

}

class Stock {
	@JacksonXmlProperty(localName = "id", isAttribute = true)
	private String id;
	@JacksonXmlProperty(localName = "type", isAttribute = true)
	private String type;
	@JacksonXmlProperty(localName = "recycle", isAttribute = true)
	private String recycle;
	@JacksonXmlProperty(localName = "denom")
	Denom denom;

	@Override
	public String toString() {
		return "Stock [id=" + id + ", type=" + type + ", recycle=" + recycle + ", denom=" + denom + "]";
	}

}

class Denom {
	@JacksonXmlProperty(localName = "curr", isAttribute = true)
	private String curr;
	@JacksonXmlProperty(localName = "value", isAttribute = true)
	private String value;

	@Override
	public String toString() {
		return "Denom [curr=" + curr + ", value=" + value + "]";
	}

}